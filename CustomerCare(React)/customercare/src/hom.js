import React from 'react'
import {useNavigate} from 'react-router-dom'
import './Forall.css'
function Hom() {
    let history=useNavigate()
    function Frontend(){
        history('/frontend')
    }
    function Backend(){
        history('/backend')
    }
  return (
    <div className='Alldiv'>
        <button onClick={Frontend}>View Frontend Issues</button>
        <br/>
        <br/>
        <br/>
        <button onClick={Backend}>View Backend Issues</button>
    </div>
  )
}

export default Hom