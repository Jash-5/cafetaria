package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/go-redis/redis"
	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var client = newPool()
var db *gorm.DB
var err error

type Orders struct {
	User_id  uuid.UUID `json:"id"`
	Order_id uuid.UUID `json:"orderId"`
	Orders   string    `json:"items"`
	Price    string    `json:"price"`
}
type Response struct {
	ID      string `json:"id,omitempty"`
	Present bool   `json:"present"`
}
type Users struct {
	User_id  uuid.UUID `json:"id"`
	Username string    `json:"username"`
	Password string    `json:"password"`
	Game     string    `json:"game"`
	Address  string 	`json:"address"`
}
type ResponseOrder struct {
	Order   []Orders `json:"orders"`
	Present bool     `json:"present"`
}
type Red struct{
	Ord  []Orders		`json:"orders"`
}
func init() {
	dsn := "host=localhost user=postgres password=root dbname=postgres port=5432 sslmode=disable TimeZone=Asia/Kolkata"
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	fmt.Println("Now we are connected to POSTGRESQL DATABASE.")
}
func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	record := Users{}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	json.NewDecoder(r.Body).Decode(&record)
	//json.NewEncoder(w).Encode(record)
	res := Response{}
	record.User_id = uuid.New()
	result := db.Select("user_id", "username", "password", "game","address").Create(&record)
	if result.Error != nil {
		res.Present = false
		json.NewEncoder(w).Encode(res)
		return
	} else {
		res.Present = true
		json.NewEncoder(w).Encode(res)
		return
	}
}

func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	record := Users{}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	json.NewDecoder(r.Body).Decode(&record)
	result := db.Where("username = ? AND password= ?", record.Username, record.Password).Find(&record)
	if result.Error != nil {
		panic(result.Error)
	}
	res := Response{}
	if result.RowsAffected == 0 {
		res.ID = "0"
		res.Present = false
		json.NewEncoder(w).Encode(res)
		return
	} else {
		res.ID = record.User_id.String()
		res.Present = true
		json.NewEncoder(w).Encode(res)
		return

	}
}
func Create(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	record := Users{}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	json.NewDecoder(r.Body).Decode(&record)
	result := db.Model(&record).Where("username=?", record.Username).Update("password", record.Password)
	if result.Error != nil {
		panic(result.Error)
	}
	res := Response{}
	if result.RowsAffected == 0 {
		res.ID = "0"
		res.Present = false
		json.NewEncoder(w).Encode(res)
		return
	} else {
		res.ID = record.User_id.String()
		res.Present = true
		json.NewEncoder(w).Encode(res)
		return

	}
}
func Change(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	record := Users{}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	json.NewDecoder(r.Body).Decode(&record)
	result := db.Where("username = ? AND game= ?", record.Username, record.Game).Find(&record)
	if result.Error != nil {
		panic(result.Error)
	}
	res := Response{}
	if result.RowsAffected == 0 {
		res.ID = "0"
		res.Present = false
		json.NewEncoder(w).Encode(res)
		return
	} else {
		res.ID = record.User_id.String()
		res.Present = true
		json.NewEncoder(w).Encode(res)
		return

	}
}
func OrdersSave(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	record := Orders{}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	json.NewDecoder(r.Body).Decode(&record)
	json.NewEncoder(w).Encode(record)
	record.Order_id = uuid.New()
	result := db.Select("user_id", "order_id", "orders", "price").Create(&record)
	if result.Error != nil {
		panic(result.Error)
	}
	var Allorders []Orders
	result1 := db.Where("user_id= ?", record.User_id).Find(&Allorders)
	if result1.Error != nil {
		panic(result1.Error)
	} else {
		json1, err1 := json.Marshal(Red{Ord:Allorders})
    if err1 != nil {
        fmt.Println(err1)
    }
		 err := client.Set(record.User_id.String(), json1,0).Err()
		if err != nil {
			panic(err)
		} 
	}
}
func GetOrders(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	order := Orders{}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	json.NewDecoder(r.Body).Decode(&order)
	var Allorders []Orders
	res := ResponseOrder{}
	value,err := client.Get( order.User_id.String()).Result()
	if  err!=redis.Nil {
		fmt.Print("From Redis Server")
		R:=Red{}
		json.Unmarshal([]byte(value),&R)
		Allorders =R.Ord
		res.Order = Allorders
		res.Present = true
		json.NewEncoder(w).Encode(res)
		return

	} else {
		result := db.Where("user_id= ?", order.User_id).Find(&Allorders)
		if result.Error != nil {
			panic(result.Error)
		}

		if result.RowsAffected == 0 {
			res.Present = false
			json.NewEncoder(w).Encode(res)
			return
		} else {
			res.Order = Allorders
			res.Present = true
			json.NewEncoder(w).Encode(res)
			return
		}
	}
}

func newPool()(*redis.Client){
	client := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
		Password: "",
		DB: 0,
    })
	return client
}

func main() {
	
	router := httprouter.New()
	router.POST("/", Index)
	router.POST("/login", Hello)
	router.POST("/order", OrdersSave)
	router.POST("/GetOrders", GetOrders)
	router.POST("/forgot", Change)
	router.POST("/Create", Create)
	log.Fatal(http.ListenAndServe(":8080", router))
}
