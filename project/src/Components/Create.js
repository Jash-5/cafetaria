import React, { useState} from 'react'
import { Form, Button} from 'react-bootstrap'
import {useHistory} from 'react-router-dom'
import Header from "./Header.js"
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import './Login.css'

function Create(props) {
  const [data, setData] = useState(
    {
      username: "",
      password: ""
    }
  )
  let history=useHistory();
  function saveData(e) {
  
   fetch("http://localhost:8080/Create", {
      method: "POST",
      headers: { 
        'Accept':'application/json',
        'Content-Type':'application/x-www-form-urlencoded' 
      },
      body: JSON.stringify(data)
    }).then(function(response){
       return response.json()
    })
    .then(function(myJson) {
      if(myJson.present===true)
      {
        localStorage.setItem('ID',myJson.id)  
        history.push('/');  
           
      }
      else{
        NotificationManager.error('New User Try Signup',"Wrong Password",700);
        history.push('/forgot/create')
      }
    });
    e.preventDefault()
}

  function handle(e) {
    const newdata = { ...data }
    newdata[e.target.id] = e.target.value
    setData(newdata)
  }
  return (
    <>
   
    <Header />
    <div className='FormStyle container my-5'>
      <Form onSubmit={saveData} >
        <Form.Group className="mb-3" controlId="username">
          <Form.Label>UserName</Form.Label>
          <Form.Control type="text" minLength="5" maxLength="30" required onChange={(e) => handle(e)} value={data.username} placeholder="Enter usename" />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password">
          <Form.Label>New Password</Form.Label>
          <Form.Control type="password" minLength="5" maxLength="30" required onChange={(e) => handle(e)} value={data.password} placeholder="Password" />
        </Form.Group>
        <br/>
        <br/>
        <Button className="styleButton" variant="success" type="submit">
          Submit
        </Button>
       
      </Form>
      <NotificationContainer/>
    </div>
    </>
  )
}

export default Create