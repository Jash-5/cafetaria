import React from 'react'
import Header from "./Header.js"
import './Myorder.css'
import {useHistory} from 'react-router-dom'

function Logout(props) {
  let history=useHistory()
    localStorage.removeItem('ID');
    function handleClick(){
      history.push('/login')
    }
  return (
      <> <Header />
    <div className='centre'>Succesfully logged out</div>
    <div className='centre'>

    <button className="btn btn-success styleButton" onClick={handleClick}>Login</button>

    </div>
    </>
  )
}

export default Logout