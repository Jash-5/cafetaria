import React from 'react'
import {Navbar,Container,Nav} from "react-bootstrap"
import {Link} from "react-router-dom";
import { FaShoppingCart} from "react-icons/fa"
import './Header.css'
function Header(props) {
 if(localStorage.getItem('ID')!=null){
  return (
    <Navbar bg="dark"  variant="dark" expand="lg">
    <Container fluid>
      <Navbar.Brand className="main"  to="/">Cafetaria</Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarScroll" />
      <Navbar.Collapse id="navbarScroll">
        <Nav
          className="me-auto my-2 my-lg-0"
          style={{ maxHeight: '100px' }}
          navbarScroll
        >
          <Link className="link"  to="/">Home</Link>
          <Link className="link"  to="/order/">My Orders</Link>
          <Link className="link"  to="/about">About</Link>
          </Nav>
        <Nav className='justify-content-end'>
        <Link className='link'  to='/cart'><FaShoppingCart /></Link>
          <Link  className="link"   to="/logout">Logout</Link>
          <Link  className="link"   to="/feedback">Feedback</Link>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
  )
 }
 else
 {
  return (
    <Navbar bg="dark"  variant="dark" expand="lg">
    <Container fluid>
      <Navbar.Brand className="main"  to="/">Cafetaria</Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarScroll" />
      <Navbar.Collapse id="navbarScroll">
        <Nav
          className="me-auto my-2 my-lg-0"
          style={{ maxHeight: '100px' }}
          navbarScroll
        >
          <Link className="link"   to="/">Home</Link>
          <Link className="link"  to="/order/">My Orders</Link>
          <Link className="link"  to="/about">About</Link>
          </Nav>
        <Nav className='justify-content-end'>
        <Link className='link' to='/cart'><FaShoppingCart/></Link>
          <Link  className="link"  to="/login">Login</Link>
         <Link  className="link"   to="/signup">Sign up</Link>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
  )
}
}
export default Header