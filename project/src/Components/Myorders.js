import React,{useState} from 'react'
import './Myorder.css'
import {useHistory} from 'react-router-dom'
import Header from "./Header.js"
import {NotificationContainer, NotificationManager} from  'react-notifications';



function Myorder() {

    const [AllOrders,setAllOrders]=useState([])
    let history=useHistory()
    async function GetOrders()
    {
      var data={
        id : localStorage.getItem('ID')
      }
      const Response = await fetch("http://localhost:8080/GetOrders", {
        method: "POST",
        headers: { 
          'Accept':'application/json',
          'Content-Type': 'application/x-www-form-urlencoded' 
        },
        body: JSON.stringify(data)
      })
      const response = await Response.json()
      setAllOrders(response.orders)
     //  console.log(AllOrders)
            NotificationManager.success('Fetched Your Orders Succesfully','Orders',700);

      }
      function Count(l,item1) {
        const movies = l.filter(item => item === item1);
        const moviesCount = movies.length;
        return moviesCount
      }
      function makeUnique(s){
         var l=s.split(" ")
          var  s1=""
         var l1=Array.from(new Set(l)).filter(String)
         for(var i=0;i<l1.length;i++){
            s1=s1+l1[i]+"("+Count(l,l1[i])+") "
         }
         return s1
      } 
    function HandleLogin()
    {
    history.push("/login")
    }
    function HandleSignup()
    {
    history.push("/signup")
    }
  
    if (localStorage.getItem('ID')!==null)
    {
      
      //NotificationManager.success('Fetched Your Orders Succesfully','Orders',700);

      if(AllOrders!==null)
      {
        
  return (
      <> 
      <Header />
     <NotificationContainer/>
    <div className='Alldiv'> 
      <button className='btn btn-success' onClick={GetOrders}> Get MY Orders</button>
      <br/>
      <br/>
    <table className="table table-striped table-bordered table-hover" variant="dark">
      <thead>
        <tr> 
        <th>Order ID</th>
        <th>Items</th>
        <th>Price</th>
        </tr>
      </thead>
      <tbody>
    
    { AllOrders.map(item => {
                  return <tr key={Math.random()}><td >{item.orderId}</td><td>{makeUnique(item.items)}</td><td>{item.price}</td></tr>
                })}
    </tbody>
    </table>
    </div>
     </>
  )
}
else{
  return(
    <>
    <Header />
    <div className='centre'>No Orders To Display</div>
    </>
  )
}
    }

else{

    
    return(
    <><Header />
    
    <div className='styleDiv centre'>
    <p >Please sign in to view your Orders</p>
    <button className="btn btn-primary styleButton" onClick={HandleLogin}type='submit'>Go to Login Page</button>
    <br></br>
    <br></br>
    <button className="btn btn-primary styleButton" onClick={HandleSignup}type='submit'>Go to signup Page</button>

    </div>
  
    </>
  
    )
}
}

export default Myorder