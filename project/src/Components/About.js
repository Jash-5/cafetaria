import React from 'react'
import Header from "./Header.js"
import  './Myorder.css'
import { FaShoppingCart} from "react-icons/fa"

function About() {
  return (
    <>
     <Header />
    <div className='Alldiv'>
      <h1> Cafetaria</h1>
      <h5 >Home</h5>
      <p>Home Page is the place for our inventory and all the menu items that we have can be seen there where you can see the images of various food items along with its names and costs in dollars with a button to add to cart where you can quickly add items you want into the cart and a green color notification can be seen on top right of page showing success message for adding into the cart.</p>
    </div>
    <div className='Alldiv'>
    <h5 >My Orders</h5>
    <p>My orders page is simple page with a button showing all your orders if you are logged in .If you are logged out then you can see a message with a login button and a signup button with these buttons on clicking you can navigate to respective pages.</p>
    </div>
    <div className='Alldiv'>
    <h5 >About</h5>
    <p>The page you are currently viewing is the about gives information about all pages.</p>
    </div>
    <div className='Alldiv'>
      <h5><FaShoppingCart/>(Cart)</h5>
    <p>This page is for viewing your cart and placing the order if you are logged in and works similarly as myorders if you are logged out.</p>
    </div>
    <div className='Alldiv'>
      <h5>Login</h5>
    <p>Log in page for authenticated user who has already signed up and want to login now you can use forget password in case you forget your password and change whenever you want.</p>
    </div>
    <div className='Alldiv'>
      <h5>Sign Up</h5>
    <p>Sign up page is for new users who want to register their username and password.</p>
    </div>
    
    </>
  )
}

export default About