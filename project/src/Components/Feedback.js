import React, { useState} from 'react'
import {  Button} from 'react-bootstrap'
import {useHistory} from 'react-router-dom'
import Header from "./Header.js"
import './Myorder.css'
import 'react-notifications/lib/notifications.css';
//import {NotificationContainer, NotificationManager} from 'react-notifications';

function Feedback(props) {
const [data,setData]=useState(
    {
    type : "",
    issue :""
    }

)
  let history=useHistory();
  function saveData(e) {
  
   fetch("http://localhost:8000/produce", {
      method: "POST",
      headers: { 
        'Accept':'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
         
      },
      body: JSON.stringify(data)
    }).then(function(response){
      history.push('/thanks')
       
    })
}
function handle(e) {
    const newdata = { ...data }
    newdata[e.target.id] = e.target.value
    setData(newdata)
  }
 
  return (
    <>
   
    <Header />
<center>
    <div className='  Alldiv'>
        <label> Select Type Of Issue   :</label>
        <select onChange={handle} id="type">
            <option value="Backend">Payment</option>
            <option value="Frontend">Website</option>
            <option value="Backend">Orders</option> 
            <option value="Frontend">Page Not Working</option> 
        </select>
        <br/>
        <br/>
        <textarea onChange={handle} rows="15" cols="50" id="issue" placeholder='Write Your Feedback here' />
        <br/>
        <br/>
        <Button  className="styleButton" onClick={saveData} variant="success" type="submit">
          Submit
        </Button>
      </div>
      </center>
    </>
  )
}

export default Feedback