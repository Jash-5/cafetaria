import React, { useState } from 'react'
import { Form, Button } from 'react-bootstrap'
import Header from "./Header.js"
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {useHistory} from 'react-router-dom'
import './Login.css'
function Signup(props) {
  let history=useHistory();

  const [data, setData] = useState(
    {
      username: "",
      password: ""
    }
  )
  async function saveData(e) {
    e.preventDefault()
    const Response = await fetch("http://localhost:8080/", {
      method: "POST",
      headers: { 
        'Accept':'application/json',
        'Content-Type': 'application/x-www-form-urlencoded' 
      },
      body: JSON.stringify(data)
    })
    const response = await Response.json()
    if(response.present===false){
    NotificationManager.warning('Username Already Exists','Try Other',700);
    }
    else
    {
        history.push('/login')
    }
  }
  function handle(e) {
    const newdata = { ...data }
    newdata[e.target.id] = e.target.value
    setData(newdata)
  }

  return (
    <> <Header />
    <NotificationContainer/>
    <div className='FormSignup container my-5'>
      <Form onSubmit={saveData}>
        <Form.Group className="mb-3" controlId="username">
          <Form.Label>UserName</Form.Label>
          <Form.Control className="box" type="text" minLength="5" maxLength="30" required onChange={(e) => handle(e)} value={data.username} placeholder="Enter usename" />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Create Password</Form.Label>
          <Form.Control className="box" type="password" minLength="5" maxLength="30" required onChange={(e) => handle(e)} value={data.password} placeholder="Password" />
        </Form.Group>

        <Form.Group className="mb-3" controlId="address">
          <Form.Label>Address</Form.Label>
          <Form.Control className="box" type="text" minLength="5" maxLength="50" required onChange={(e) => handle(e)} value={data.address} placeholder="Address" />
        </Form.Group>
        
        <Form.Group className="mb-3" controlId="game">
          <Form.Label>Favourite Game(This Field is used to retreive password incase you forget)</Form.Label>
          <Form.Control className="box" type="text" minLength="2" maxLength="30" required onChange={(e) => handle(e)} value={data.game} placeholder="..." />
        </Form.Group>

       
        <Button  variant="success" type="submit">
          Submit
        </Button>
      </Form>
    </div>
    </>
  )
}

export default Signup