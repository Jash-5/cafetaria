import React, { useState } from 'react'
import './Cart.css'
import { useHistory } from 'react-router-dom'
import Header from "./Header.js"
import {Dropdown,DropdownButton} from 'react-bootstrap'
import { NotificationContainer, NotificationManager } from 'react-notifications';
function Cart(props) {
  let history = useHistory()
  var l
  var TotalPrice = 0
  var id = localStorage.getItem('ID')
  var s = localStorage.getItem(id)
  if (s != null) {
    l = s.split(" ")
  } else {
    l = []
  }
  l = l.filter(e => e)
  for (const [, it] of l.entries()) {
    TotalPrice += parseInt(localStorage.getItem(it))
  }
  const [list, setList] = useState(Array.from(new Set(l)))
  function HandleLogin() {
    history.push("/login")
  }
  function HandleSignup() {
    history.push("/signup")
  }
  function HandleHome() {
    history.push("/")
  }
  function PlaceOrder() {
    NotificationManager.success('Order Placed Successfully you can view it in Myorders', 'Order placed', 1500);
    var data = {
      "id": localStorage.getItem('ID'),
      "items": localStorage.getItem(localStorage.getItem('ID')),
      "price": TotalPrice + "$"
    }
    fetch("http://localhost:8080/order", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: JSON.stringify(data)
    }).then(async Response => {
      const data = await Response.json();
      if (!Response.ok) {
        const error = (data && data.message) || Response.status
        return Promise.reject(error);
      }
    }
    )
    localStorage.setItem(id, "")
  }
  const removeItem = (arr, item) => {
    let newArray = [...arr]
    const index = newArray.findIndex((element) => element === item)
    newArray.splice(index, 1)
    return newArray
  }
  function calculateList(item) {
    var n = Count(item)
    var w =Array.from(Array(n + 1).keys())
    w.splice(0,1)
    return w
  }
  function Add(item) {
    var s1 = localStorage.getItem(id)
    s1 = s1 + " " + item
    localStorage.setItem(id, s1)
    l = s1.split(" ")
    setList(Array.from(new Set(l)))
  }
  function Count(item1) {
    const movies = l.filter(item => item === item1);
    const moviesCount = movies.length;
    return moviesCount
  }
  function CalculateCost(item1) {
    return localStorage.getItem(item1) * Count(item1)
  }
  if (localStorage.getItem('ID') !== null) {
    function Delete(item, n) {
      while (n !== 0) {
        l = removeItem(l, item)
        n -= 1
      }
      var s1 = "";

      for (const [, ind] of l.entries()) {
        s1 += ind + " "
      }
      s1 = s1.slice(0, -1)
      localStorage.setItem(id, s1);
      setList(Array.from(new Set(l)))
    }
    if (l.length !== 0) {
      return (
        <>
          <Header />
          <NotificationContainer />
          <div className='container my-5'>
            <table className="table table-striped table-bordered table-hover" variant="dark">
              <thead>
                <tr>
                  <th>
                    ITEMS
                  </th>
                  <th>
                    INDIVIDUAL PRICE
                  </th>
                  <th>
                    PRICES
                  </th>
                  <th>
                    ITEM COUNT
                  </th>
                  <th>
                    </th>
                </tr>
              </thead>
              <tbody>

                {list.map(item => {
                  return <tr key={Math.random()}><td> {item} </td><td>{localStorage.getItem(item)}$</td><td>{CalculateCost(item)}$</td><td>{Count(item)}</td>

                    <td>
                      <DropdownButton id="dropdown-item-button"  variant="outline-warning" title="Delete" >
                        {calculateList(item).map(i => {
          
                          return <Dropdown.Item as="button"  onClick={(e) => Delete(item, e.target.value)} key={i} value={i}>Delete {i}</Dropdown.Item>
                        })}
                      </DropdownButton>
                      


                      <button  style={{ margin: '1rem' }} type="submit" onClick={Add.bind(this, item)} className="btn btn-outline-success styleButton" value="Add" >Add</button></td></tr>;
                })}
              </tbody>
              <tfoot>
                <tr>
                  <td>
                  Total Price
                  </td>
                  <td></td>
                  <td>
                    {TotalPrice + "$"}
                  </td>
                <td>
                  {l.length}
                </td>
                  <td>
                    <button  type="submit" onClick={PlaceOrder} className="btn btn-success styleButton">Place Order</button>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </>
      )
    }
    else {
      return (
        <><Header />
          <div className='centre'>
            <p >No Items in the Cart</p>
            <button className="btn btn-primary" onClick={HandleHome} type='submit'>Add From Home </button>
          </div>
        </>
      )
    }
  }
  else {
    return (
      <><Header />
        <div className='styleDiv centre'>
          <p >Please sign in to view your cart</p>
          <button className="btn btn-primary styleButton" onClick={HandleLogin} type='submit'>Go to Login Page</button>
          <br></br>
          <br></br>
          <button className="btn btn-primary styleButton" onClick={HandleSignup} type='submit'>Go to signup Page</button>

        </div>
      </>
    )
  }
}

export default Cart